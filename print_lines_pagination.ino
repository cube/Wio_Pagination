// Import and define the screen
#include "TFT_eSPI.h"
TFT_eSPI tft;

const int number_of_lines = 45; // Number of lines of fake data to generate
String lines[number_of_lines]; // Create the array
int max_per_page = 10; // How many items can be displayed on one page
int page = 1; // Start on page 1
bool last_page = false; // Used to prevent scrolling past the last page

// Function for adding data to the array of lines
void generate_lines()
{
  for(int l = 0; l < number_of_lines; l++)
  {
    lines[l] = "Line " + (String)(l+1);
  }
}

// Function to show the current page of lines
void show_page()
{
  // Reset screen, text starting position & font color
  blank_to_title();
  int textpos = 30;
  tft.setTextColor(TFT_WHITE);

  int offset = (page - 1) * max_per_page; // Index of the first item on this page
  int max_offset = (page) * max_per_page; // Index of the last item on this page

  // If the last item on this page is greater than
  // or equal to the last item in the array, then it is the last page.
  if(max_offset >= number_of_lines) 
  {
    max_offset = number_of_lines;
    last_page = true;
  }
  else
  {
    last_page = false;
  }

  // Loop through the list of lines, using the offsets
  for(int x = offset; x < max_offset; x++) 
  {
    tft.drawString(lines[x], 10, textpos); // Print the string
    textpos += 20; // Tweak this according to how spaced out you want each line to be.
  }
}

// Function that clears the screen, and prints the title
// In this case, the title is the page number
void blank_to_title()
{
  tft.fillScreen(TFT_BLACK); // Clear the screen  to black
  tft.setTextColor(TFT_GREENYELLOW); // Title colour
  tft.drawString("Page "+(String)page, 10, 10); // Print the title
}

void setup() {
  // Set up Buttons
  pinMode(WIO_5S_LEFT, INPUT_PULLUP);
  pinMode(WIO_5S_RIGHT, INPUT_PULLUP);

  tft.begin(); // Set up screen
  tft.setRotation(3); // Changing this requires potentially changing the button definitions and/or directions
  tft.setFreeFont(&FreeSans9pt7b); // Set the font
  blank_to_title(); // Call this to set the screen color and print the title

  // Set up the array and show the first page
  generate_lines();
  show_page();
}

void loop() {
  // When the left arrow is pressed
  if (digitalRead(WIO_5S_LEFT) == LOW) {
    if(page == 1){} // Don't do anything if we're on the first page
    else
    {
      // Otherwise go back a page
      page--;
      show_page();
      delay(500);
    }
   }
  // When the right arrow is pressed
  else if (digitalRead(WIO_5S_RIGHT) == LOW) {
    if(last_page){} // Don't do anything if we're on the last page
    else
    {
      // Otherwise go forward a page
      page++;
      show_page();
      delay(500);
    }
  }
}
