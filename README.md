# README #
Template for printing an array of Strings (lines) to the Seeed Wio Terminal display, using the left and right directional keys to navigate between pages. The user cannot go to a page less than 1, and they cannot go to a page beyond the maximum number of items in the list.

This project uses an array of String items, iteratively generated to demonstrate the concept. In a project that might require a long list of items to be displayed on this small screen, it can be helpful for the user to be able to navigate through pages of items.

This could be used as the guide for building a program that can load a text file from the SD card and display it, allowing the user to scroll through the document.